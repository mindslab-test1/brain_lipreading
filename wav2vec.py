import os
import glob
import tqdm
import librosa
import torch

from argparse import ArgumentParser
from transformers import Wav2Vec2ForPreTraining


def main(args):
    model = Wav2Vec2ForPreTraining.from_pretrained(args.checkpoint_path).cuda()

    wavpath_discriminator = glob.glob(os.path.join('/DATA', '**', '*.wav'), recursive=True)

    for wavpath in tqdm.tqdm(wavpath_discriminator):

        input_wav, _ = librosa.load(wavpath, sr=16000)
        input_tensor = torch.from_numpy(input_wav).unsqueeze(0).cuda()
        outputs = model(input_tensor, output_hidden_states=True)[3][args.layer_index].cpu().detach().squeeze(0)

        torch.save(outputs, wavpath.replace('.wav','.feat'))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-p', '--checkpoint_path', default='facebook/wav2vec2-large-xlsr-53', type=str,
                        help="path of checkpoint for resuming")
    parser.add_argument('-l', '--layer_index', type=int, default=24,
                        help="layer index of wav2vec 2.0")
    args = parser.parse_args()

    main(args)
