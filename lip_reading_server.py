import io
import time
import argparse
import grpc
import logging
import torch
import torch.nn.functional as F

from concurrent import futures

from compute_posterior_gram import W2lClient
from model import Lipreading_Model

from maum.brain.w2l.w2l_pb2 import Speech
from maum.brain.lipreading.lipreading_pb2 import EvaluationResult
from maum.brain.lipreading.lipreading_pb2_grpc import add_LipReadingServicer_to_server, LipReadingServicer


def parse_args():
    parser = argparse.ArgumentParser(description='lip reading server')
    # general
    parser.add_argument('-p', '--port', default=55101, type=int, help='server port')
    parser.add_argument('--log_level', default='INFO', type=str, help='logger level')
    # LipReading config
    parser.add_argument('-w', '--w2l_remote', default='127.0.0.1:15001', type=str, help='w2l ip:port')
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="path of configuration yaml file")
    parser.add_argument('-m', '--checkpoint_path', type=str, default=None,
                        help="path of checkpoint for resuming")

    return parser.parse_args()


class LipReadingServer(LipReadingServicer):
    def __init__(self, config, checkpoint_path, w2l_remote):
        parser = argparse.ArgumentParser()
        parser.add_argument('--config')
        args = parser.parse_args(['--config', config])
        self.net = Lipreading_Model.load_from_checkpoint(
            checkpoint_path,
            map_location='cpu',
            hparams=args
        )
        self.net.cuda().eval()
        self.net.freeze()

        self.word_dict = {
            word: idx for idx, word in enumerate(self.net.hp.data.words)
        }

        self.w2l_client = W2lClient(w2l_remote)

    @staticmethod
    def _make_speech_iter(request_iterator, text_io):
        for pronunciation in request_iterator:
            text_io.write(pronunciation.text)
            yield Speech(bin=pronunciation.speech)

    def EvaluatePronunciation(self, request_iterator, context):
        try:
            with io.StringIO() as text_io:
                speech_iter = self._make_speech_iter(request_iterator, text_io)

                ppg = self.w2l_client.compute_posterior_gram(speech_iter)
                ppg = ppg.unsqueeze(0)
                ppg = ppg.cuda()

                text = text_io.getvalue()
                if text not in self.word_dict:
                    context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                    context.set_details(f'"{text}" is unknown word.')
                    return EvaluationResult()
                word = self.word_dict[text]
                word = [word]
                word = torch.LongTensor(word)
                word = word.cuda()

            result = self.net(word, ppg)
            result = F.softmax(result, dim=1)
            return EvaluationResult(score=result[0][1].item())
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))


if __name__ == "__main__":
    args = parse_args()
    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    lip_reading_server = LipReadingServer(
        args.config, args.checkpoint_path, args.w2l_remote)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=30),)
    add_LipReadingServicer_to_server(lip_reading_server, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.info('lipreading starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)
