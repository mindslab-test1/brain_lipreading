from tqdm import tqdm
from contextlib import closing
from multiprocessing import Pool


def parallel_run(fn, items, desc="", n_cpu=1):
    results = []

    if n_cpu > 1:
        with closing(Pool(n_cpu)) as pool:
            for out in tqdm(pool.imap_unordered(
                    fn, items), total=len(items), desc=desc):
                if out is not None:
                    results.append(out)
    else:
        for item in tqdm(items, total=len(items), desc=desc):
            out = fn(item)
            if out is not None:
                results.append(out)

    return results
