import random
import torch
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.utilities import rank_zero_only


class LipreadingLogger(TensorBoardLogger):
    def __init__(self, save_dir, name='default', version=None, **kwargs):
        super().__init__(save_dir, name, version, **kwargs)

    @rank_zero_only
    def log_loss(self, loss, mode, step, name=''):
        name = '.loss' + ('' if name=='' else '_'+name)
        self.experiment.add_scalar(mode + name, loss, step)

    @rank_zero_only
    def log_embedding(self, symbols, embedding, step):
        self.experiment.add_embedding(
            mat=embedding,
            metadata=symbols,
            global_step=step,
            tag='character_embedding')

    @rank_zero_only
    def log_learning_rate(self, learning_rate, step):
        self.experiment.add_scalar('learning_rate', learning_rate, step)
