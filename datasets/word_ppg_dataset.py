import os
import re
import torch
import random
import librosa
import numpy as np
from torch.utils.data import Dataset
from collections import Counter

class Word_PPG_Dataset(Dataset):
    def __init__(self, hp, data, data_dir, metadata_path, train=True):
        super().__init__()
        self.hp = hp
        self.train = train
        self.data = data
        self.data_dir = data_dir
        self.meta = []
        self.ans_dict = {'good': 1, 'bad': 0}
        #self.rule_dict = {'f': 0, 'l': 1, 'r': 2, 'th': 3, 'v': 4}
        self.word_dict = {word: idx for idx, word in enumerate(hp.data.words)}
        self.meta = self.load_metadata(data_dir, metadata_path)

        if train:
            word_counter = Counter((word \
                                       for audiopath, word, spk_id, ans, rule in self.meta))
            weights = [1.0 / word_counter[word] \
                       for audiopath, word, spk_id, ans, rule in self.meta]
            self.mapping_weights = torch.DoubleTensor(weights)


    def __len__(self):
        return len(self.meta)

    def __getitem__(self, idx):

        if self.train:
            idx = torch.multinomial(self.mapping_weights, 1).item()

        audiopath, word, spk_id, ans, rule = self.meta[idx]
        ans = self.ans_dict[ans]

        audiopath = os.path.join(self.data_dir, audiopath)
        ppg = self.get_ppg(audiopath)
        word_idx = self.word_dict[word]

        return word_idx, ppg, spk_id, ans

    def get_ppg(self, audiopath):
        ppgpath = os.path.join(self.data_dir, audiopath)
        ppgpath = ppgpath.replace('.ppg','.feat')
        ppg = torch.load(ppgpath, map_location='cpu') # PPG: [Time, number of tokens] -> [T,N]

        return ppg

    def load_metadata(self, data_dir, path, split="|"):
        path = os.path.join(data_dir, path)
        with open(path, 'r', encoding='utf-8') as f:
            metadata = []
            for line in f:
                t = line.strip().split(split)
                path, word, rule, ans, speaker = t
                metadata.append((path,word,speaker,ans,rule))

        return metadata


class word_ppg_collate():

    def __init__(self, n_frames_per_step):
        self.n_frames_per_step = n_frames_per_step

    def __call__(self, batch):
        output_lengths, ids_sorted_decreasing = torch.sort(
            torch.LongTensor([x[1].size(0) for x in batch]),
            dim=0, descending=True)

        n_mel_channels = batch[0][1].size(1)
        #max_input_len = max([len(x[0]) for x in batch])
        #input_lengths = torch.empty(len(batch), dtype=torch.long)
        max_target_len = max([x[1].size(0) for x in batch])

        words = torch.empty(len(batch), dtype=torch.long)
        ppg_padded = torch.zeros(len(batch), max_target_len, n_mel_channels)
        #speakers = torch.empty(len(batch), dtype=torch.long)
        answers = torch.empty(len(batch), dtype=torch.long)

        for idx, key in enumerate(ids_sorted_decreasing):
            words[idx] = batch[key][0]
            ppg = batch[key][1]
            ppg_padded[idx, :ppg.size(0), :] = ppg
            #speakers[idx] = batch[key][2]
            answers[idx] = batch[key][3]

        return words, ppg_padded, answers, output_lengths

