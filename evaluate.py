import os
import tqdm
import torch
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from argparse import ArgumentParser
from omegaconf import OmegaConf

from utils.utils import get_commit_hash
from model import Lipreading_Model
from utils.loggers import LipreadingLogger
from datasets import Word_PPG_Dataset, word_ppg_collate


def main(args):
    hp = OmegaConf.load(args.config)
    print('Loaded hparams from %s' % args.config)

    model = Lipreading_Model(args).cuda()
    checkpoint = torch.load(args.checkpoint_path)
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()
    model.freeze()
    print('Finished loading the model from %s' % args.checkpoint_path)

    testset = Word_PPG_Dataset(hp, hp.data, hp.data.val_dir, args.metadata_path, False)
    testloader = DataLoader(testset, batch_size=hp.train.batch_size, shuffle=False,
                    num_workers=hp.train.num_workers,
                    collate_fn=word_ppg_collate(1), pin_memory=False, drop_last=False)

    correct = 0
    for batch in tqdm.tqdm(testloader):

        text, ppg, answers, output_lengths = batch

        text = text.cuda()
        ppg = ppg.cuda()
        answers = answers.cuda()
        output_lengths = output_lengths.cuda()

        z = model(text, ppg)

        pred = z.argmax(dim=1)
        answers = answers.cuda()
        correct += pred.eq(answers.view_as(pred)).float().sum().item()


    accuracy = correct / len(testset)
    print('Accuracy: %.5f' %accuracy)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="path of configuration yaml file")
    parser.add_argument('-p', '--checkpoint_path', type=str, required=True,
                        help="path of checkpoint for resuming")
    parser.add_argument('-m', '--metadata_path', type=str, required=True,
                        help="path of metadata for test")
    args = parser.parse_args()

    with torch.no_grad():
        main(args)
