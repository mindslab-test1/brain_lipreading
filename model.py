import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, ConcatDataset
import pytorch_lightning as pl
import random
import numpy as np
from omegaconf import OmegaConf

from modules import Classifier
from datasets import Word_PPG_Dataset, word_ppg_collate

class Lipreading_Model(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        self.save_hyperparameters()  # used for pl
        hp = OmegaConf.load(hparams.config)
        self.hp = hp

        self.word_embedding = nn.Embedding(len(self.hp.data.words), hp.chn.ppg)
        self.classifier = Classifier(2 * hp.chn.ppg, len(hp.data.words))

        self.is_val_first = True

        self.collate_fn = word_ppg_collate(1)

    def forward(self, text, ppg):
        text = self.word_embedding(text)
        text = text.unsqueeze(1).expand(-1, ppg.size(1), -1) # [B, T, C]
        ppg = torch.cat([ppg, text], dim=2)

        z = self.classifier(ppg)

        return z

    def training_step(self, batch, batch_idx):
        text, ppg, answers, output_lengths = batch

        z = self(text, ppg)

        loss = F.nll_loss(z, answers)

        self.logger.log_loss(loss, mode='train', step=self.global_step)
        return {'loss': loss}

    def validation_step(self, batch, batch_idx):
        text, ppg, answers, output_lengths = batch

        z = self(text, ppg)

        loss = F.nll_loss(z, answers)

        all_results = (torch.argmax(z, dim=1) - answers)
        err = torch.count_nonzero(all_results).item()
        tot = answers.size(0)

        return {'loss': loss, 'err': err, 'tot': tot}

    def validation_epoch_end(self, outputs):
        loss = torch.stack([x['loss'] for x in outputs]).mean()
        self.log('val_loss', loss)

        acc = 1. * sum([x['err'] for x in outputs])/sum([x['tot'] for x in outputs])
        acc = (1.-acc)*100
        self.log('accuracy', acc)

    def configure_optimizers(self):
        learnable_params = self.parameters()
        return torch.optim.Adam(
            learnable_params,
            lr=self.hp.train.adam.lr,
            weight_decay=self.hp.train.adam.weight_decay,
        )

    def lr_lambda(self, step):
        progress = (step - self.hp.train.decay.start) / (self.hp.train.decay.end - self.hp.train.decay.start)
        return self.hp.train.decay.rate ** np.clip(progress, 0.0, 1.0)

    def optimizer_step(self, epoch, batch_idx, optimizer, optimizer_idx, optimizer_closure=None, on_tpu=False, using_native_amp=False, using_lbfgs=False):
        lr_scale = self.lr_lambda(self.global_step)
        for pg in optimizer.param_groups:
            pg['lr'] = lr_scale * self.hp.train.adam.lr

        optimizer.step()
        optimizer.zero_grad()

        self.logger.log_learning_rate(lr_scale * self.hp.train.adam.lr, self.global_step)

    def train_dataloader(self):
        trainset = Word_PPG_Dataset(self.hp, self.hp.data, self.hp.data.train_dir, self.hp.data.train_meta, True)
        return DataLoader(trainset, batch_size=self.hp.train.batch_size, shuffle=True,
                        num_workers=self.hp.train.num_workers,
                        collate_fn=self.collate_fn, pin_memory=True, drop_last=True)

    def val_dataloader(self):
        valset = Word_PPG_Dataset(self.hp, self.hp.data, self.hp.data.val_dir, self.hp.data.val_meta, False)
        return DataLoader(valset, batch_size=self.hp.train.batch_size, shuffle=False,
                        num_workers=self.hp.train.num_workers,
                        collate_fn=self.collate_fn, pin_memory=False, drop_last=False)

