import grpc
import argparse

from maum.brain.lipreading.lipreading_pb2 import Pronunciation
from maum.brain.lipreading.lipreading_pb2_grpc import LipReadingStub


class LipReadingClient(object):
    def __init__(self, remote, chunk_size=16*1024):
        self.channel = grpc.insecure_channel(remote)
        self.stub = LipReadingStub(self.channel)
        self.chunk_size = chunk_size

    def _generate_pronunciation_iterator(self, wav_binary, text):
        for idx in range(0, len(wav_binary), self.chunk_size):
            in_text = text if idx == 0 else ''
            yield Pronunciation(
                speech=wav_binary[idx:idx+self.chunk_size],
                text=in_text
            )

    def evaluate_pronunciation(self, wav_binary, text):
        pronunciation_iterator = self._generate_pronunciation_iterator(
            wav_binary, text)
        return self.stub.EvaluatePronunciation(pronunciation_iterator)


def main():
    parser = argparse.ArgumentParser(
        description='lip reading client')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='127.0.0.1:55101')
    parser.add_argument('-w', '--wav',
                        nargs='?',
                        help='input wav file',
                        type=str,
                        required=True)
    parser.add_argument('-t', '--text',
                        nargs='?',
                        help='input text',
                        type=str,
                        required=True)

    args = parser.parse_args()

    lip_reading_client = LipReadingClient(args.remote)
    with open(args.wav, 'rb') as rf:
        wav_binary = rf.read()
    result = lip_reading_client.evaluate_pronunciation(wav_binary, args.text)
    print(result)


if __name__ == '__main__':
    main()
