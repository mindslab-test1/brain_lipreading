import torch
import torch.nn as nn
import torch.nn.functional as F

class Classifier(torch.nn.Module):
    def __init__(self, input_dim, output_dim):
        super(Classifier, self).__init__()

        self.cnn = nn.Sequential(
            nn.Conv1d(input_dim, 256, kernel_size=15, padding=7, stride=2),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Conv1d(256, 256, kernel_size=7, padding=3, stride=2),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Conv1d(256, 256, kernel_size=7, padding=3, stride=2),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Conv1d(256, 256, kernel_size=7, padding=3, stride=2),
        )
        self.maxpool = nn.AdaptiveAvgPool1d(1)
        self.fc2 = nn.Sequential(
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(256, 256),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(256, 2),
        )


    def forward(self, x):
        x = x.transpose(1,2)
        x = self.cnn(x)
        x = self.maxpool(x)
        x = x.squeeze(-1)
        z = self.fc2(x)
        z = F.log_softmax(z, dim=1)
        return z
